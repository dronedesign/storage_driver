var S3Driver = require("./S3Driver.js");
var FileSystemDriver = require("./FileSystemDriver.js");

module.exports = function StorageDriver(handlerType, options) {

    var handlerObject = null;
    var handlerMode = handlerType;

    if (typeof options === 'object') {
        switch (handlerType) {
            case "s3":
                this.handlerObject = new S3Driver(options);
                break;
            case "fs":
                this.handlerObject = new FileSystemDriver(options);
                break;
            default:
                throw new Exception("Unknow filesystem handler type");
                break;
        }
    }


    this.uploadFileFromBuffer = function (ufOptions) {
        return this.handlerObject.uploadFileFromBuffer(ufOptions);
    };

    this.copyFile = function (cpOptions) {
        return this.handlerObject.copyFile(cpOptions);
    };

    this.downloadFile = function (dlOptions) {
        return this.handlerObject.downloadFile(dlOptions);
    }


    this.deleteFile = function (dlOptions) {
        return  this.handlerObject.deleteFile(dlOptions);
    };


    this.copyDirectory = function (cpOptions) {
        return  this.handlerObject.copyDirectory(cpOptions);
    };

    this.deleteDirectory = function (dlOptions) {
        return  this.handlerObject.deleteDirectory(dlOptions);
    };

    this.importDataDirectory = function (idOptions) {
        return this.handlerObject.importDataDirectory(idOptions);
    };

    this.listFileInFolder = function (lsOptions) {
        return this.handlerObject.listFileInFolder(lsOptions);
    };

    this.setBucketCors = function (sbcOptions) {
        return this.handlerObject.setBucketCors(sbcOptions);
    };

    this.setBucketPolicy = function (sbpOptions) {
        return this.handlerObject.setBucketPolicy(sbpOptions);
    };

    this.getSignedUrlSync = function (gsusOptions) {
        return this.handlerObject.getSignedUrlSync(gsusOptions);
    }

    this.getFileStats = function (gfsOptions) {
        return this.handlerObject.getFileStats(gfsOptions);
    };

    return this;
};