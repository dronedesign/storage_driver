var s3 = require("s3");
var AWS = require('aws-sdk');


module.exports = function S3Driver(s3Options) {

    this.s3Client = null;
    this.awsS3Client = null;

    if (typeof s3Options === 'object') {
        this.s3Client = s3.createClient({
            s3Options: {
                accessKeyId: s3Options.accessKeyId,
                secretAccessKey: s3Options.secretAccessKey,
                endpoint: s3Options.endpoint,
                sslEnabled: s3Options.sslEnabled ? s3Options.sslEnabled : true
            }
        });

        this.awsS3Client = new AWS.S3({
            accessKeyId: s3Options.accessKeyId,
            secretAccessKey: s3Options.secretAccessKey,
            endpoint: s3Options.endpoint,
        });
    }



    this.downloadFile = function (options) {

        var bucketName = options.bucketName;
        var filePath = options.filePath;
        var downloadDest = options.downloadDest;
        var progressCallback = options.progressCallback;
        var endCallback = options.endCallback;
        var errorCallback = options.errorCallback;

        var params = {
            localFile: downloadDest,

            s3Params: {
                Bucket: bucketName,
                Key: filePath,
                // other options supported by getObject
                // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#getObject-property
            },
        };

        var downloader = this.s3Client.downloadFile(params);
        downloader.on('error', function (err) {
            errorCallback(err, downloader);
        });
        downloader.on('progress', function () {
            progressCallback(downloader);
        });
        downloader.on('end', function () {
            endCallback(downloader);
        });
    }

    this.uploadFileFromBuffer = function (options) {

        var body = options.body;
        var bucketName = options.bucketName;
        var bucketFolderPath = options.bucketFolderPath;
        //var progressCallback = options.progressCallback;
        var endCallback = options.endCallback;
        var errorCallback = options.errorCallback;
        var ACL = options.ACL ? options.ACL : "private";
        var metadata = options.metadata ? options.metadata : {};


        var s3Params = {
            Bucket: bucketName,
            Key: bucketFolderPath,
            ACL: ACL,
            Metadata: metadata,
            Body: body,
        }


        if (options.ContentDisposition) {
            s3Params["ContentDisposition"] = options.ContentDisposition;
        }

        if (options.ContentType) {
            s3Params["ContentType"] = options.ContentType;
        }
        
        if (options.ContentEncoding) {
            s3Params["ContentEncoding"] = options.ContentEncoding;
        }


        this.awsS3Client.putObject(s3Params, (err, data) => {
            if (err) {
                errorCallback(err);
            }
            endCallback(data);
        });


    };

    this.copyFile = function (options) {

        var filePath = options.filePath;
        var bucketName = options.bucketName;
        var bucketFolderPath = options.bucketFolderPath;
        var progressCallback = options.progressCallback;
        var endCallback = options.endCallback;
        var errorCallback = options.errorCallback;
        var ACL = options.ACL ? options.ACL : "private";
        var metadata = options.metadata ? options.metadata : {};

        var params = {
            localFile: filePath,
            s3Params: {
                Bucket: bucketName,
                Key: bucketFolderPath,
                ACL: ACL,
                Metadata: metadata
                        // other options supported by putObject, except Body and ContentLength. 
                        // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property                 
            }
        };

        if (options.ContentDisposition) {
            params.s3Params["ContentDisposition"] = options.ContentDisposition;
        }

        var uploader = this.s3Client.uploadFile(params);

        uploader.on('error', function (error) {
            errorCallback(error, uploader);
        });
        uploader.on('progress', function () {
            progressCallback(uploader);
        });
        uploader.on('end', function () {
            endCallback(uploader);
        });

    };

    this.copyDirectory = function (options) {

        var dirPath = options.dirPath;
        var bucketName = options.bucketName;
        var bucketDirectory = options.bucketDirectory;
        var progressCallback = options.progressCallback;
        var endCallback = options.endCallback;
        var errorCallback = options.errorCallback;
        var ACL = options.ACL ? options.ACL : "private";

        var params = {
            localDir: dirPath,
            deleteRemoved: true, // default false, whether to remove s3 objects              // that have no corresponding local file. 
            s3Params: {
                Bucket: bucketName,
                Prefix: bucketDirectory,
                ACL: ACL
                        // other options supported by putObject, except Body and ContentLength. 
                        // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property 
            }
        };

        if (options.ContentDisposition) {
            params.s3Params["ContentDisposition"] = options.ContentDisposition;
        }

        if (options.getS3ParamsCallback) {
            params["getS3Params"] = options.getS3ParamsCallback;
        }

        var uploader = this.s3Client.uploadDir(params);

        uploader.on('error', function (error) {
            errorCallback(error, uploader);
        });
        uploader.on('progress', function () {
            progressCallback(uploader);
        });
        uploader.on('end', function () {
            endCallback(uploader);
        });
    };


    this.deleteDirectory = function (options) {

        var bucketName = options.bucketName;
        var bucketDirectory = options.bucketDirectory;
        var progressCallback = options.progressCallback;
        var endCallback = options.endCallback;
        var errorCallback = options.errorCallback;

        var params = {
            Bucket: bucketName,
            Prefix: bucketDirectory,
            s3Params: {
            }
        };

        var uploader = this.s3Client.deleteDir(params);

        uploader.on('error', function (error) {
            errorCallback(error, uploader);
        });
        uploader.on('progress', function () {
            progressCallback(uploader);
        });
        uploader.on('end', function () {
            endCallback(uploader, bucketDirectory);
        });

    };

    this.deleteFile = function (options) {

        var bucketName = options.bucketName;
        var deleteObject = options.deleteObject;
        var progressCallback = options.progressCallback;
        var dataCallback = options.dataCallback;
        var endCallback = options.endCallback;
        var errorCallback = options.errorCallback;

        var params = {
            Delete: {Objects: deleteObject},
            Bucket: bucketName,
            s3Params: {

            }
        };

        var uploader = this.s3Client.deleteObjects(params);

        uploader.on('error', function (error) {
            errorCallback(error, uploader);
        });
        uploader.on('data', function (data) {
            dataCallback(data, uploader);
        });
        uploader.on('progress', function () {
            progressCallback(uploader);
        });
        uploader.on('end', function () {
            endCallback(uploader);
        });
    };


    this.importDataDirectory = function (options) {

        var dirPath = options.dirPath;
        var bucketName = options.bucketName;
        var bucketDirectory = options.bucketDirectory;
        var progressCallback = options.progressCallback;
        var endCallback = options.endCallback;
        var errorCallback = options.errorCallback;

        var params = {
            localDir: dirPath,
            deleteRemoved: true,

            s3Params: {
                Bucket: bucketName,
                Prefix: bucketDirectory
                        // other options supported by putObject, except Body and ContentLength. 
                        // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property 
            }
        };

        var uploader = this.s3Client.downloadDir(params);

        uploader.on('error', function (error) {
            errorCallback(error, uploader);
        });
        uploader.on('progress', function () {
            progressCallback(uploader);
        });
        uploader.on('end', function () {
            endCallback(uploader);
        });

    };

    this.listFileInFolder = function (options) {

        var bucketName = options.bucketName;
        var prefix = options.prefix;
        var recursive = options.recursive ? options.recursive : true;
        var dataCallback = options.dataCallback;
        var endCallback = options.endCallback;
        var errorCallback = options.errorCallback;

        var params = {
            recursive: recursive,
            s3Params: {
                Bucket: bucketName,
                Prefix: prefix
                        // other options supported by putObject, except Body and ContentLength. 
                        // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property 
            }
        };

        var uploader = this.s3Client.listObjects(params);

        uploader.on('error', function (error) {
            errorCallback(error, uploader);
        });
        uploader.on('data', function (data) {
            dataCallback(data, uploader);
        });
        uploader.on('end', function () {
            endCallback(uploader);
        });
    };


    this.setBucketCors = function (options) {

        var bucketName = options.bucketName;
        var corsConfiguration = options.corsConfiguration;
        var errorCallback = options.errorCallback;
        var successCallback = options.successCallback;

        var params = {
            Bucket: bucketName,
            CORSConfiguration: corsConfiguration
        };

        this.awsS3Client.putBucketCors(params, (err, data) => {
            if (err) {
                errorCallback(err);
            }
            successCallback(data);
        });

    };

    this.setBucketPolicy = function (options) {

        var bucketName = options.bucketName;
        var bucketPolicy = options.bucketPolicy;
        var errorCallback = options.errorCallback;
        var successCallback = options.successCallback;

        var params = {
            Bucket: bucketName,
            Policy: bucketPolicy
        };

        this.awsS3Client.putBucketPolicy(params, (err, data) => {
            if (err) {
                errorCallback(err);
            }
            successCallback(data);
        });

    }


    this.getSignedUrlSync = function (options) {

        var bucketName = options.bucketName;
        var objectKey = options.objectKey;
        var expireSecond = options.expireSecond;

        var params = {
            Bucket: bucketName,
            Key: objectKey,
            Expires: expireSecond
        };

        var url = this.awsS3Client.getSignedUrl('getObject', params);
        return url;
    }

    this.getFileStats = function (options) {

        var bucketName = options.bucketName;
        var key = options.filename;
        var dataCallback = options.dataCallback;
        var errorCallback = options.errorCallback;

        var params = {
            s3Params: {
                Bucket: bucketName,
                Key: key
                        // other options supported by putObject, except Body and ContentLength. 
                        // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property 
            }
        };

        this.s3Client.s3.headObject(params.s3Params, function (err, data) {

            if (err) {
                errorCallback(err);
                return;
            }

            data["key"] = key;
            dataCallback(data);
        });

    };


    return this;

};